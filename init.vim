noremap h i
noremap i k
noremap k j
noremap j h
noremap e ,
noremap w .
noremap . w
noremap e $
inoremap kj <esc>

set nowrap
set tabstop=4
set shiftwidth=4

autocmd BufReadPost *.svelte set filetype=html